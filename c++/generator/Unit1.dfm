object Form1: TForm1
  Left = 254
  Top = 73
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Generator of pseudorandom numbers'
  ClientHeight = 681
  ClientWidth = 1311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo: TMemo
    Left = 1072
    Top = 16
    Width = 185
    Height = 481
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 0
    Width = 1009
    Height = 681
    ActivePage = TabSheet5
    TabIndex = 4
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1051#1072#1073' 3'
      object Chart7: TChart
        Left = 33
        Top = 267
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'puasson')
        TabOrder = 0
        object Series7: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart8: TChart
        Left = 32
        Top = 0
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'puasson numbers')
        TabOrder = 1
        object Series8: TPointSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Button2: TButton
        Left = 464
        Top = 248
        Width = 75
        Height = 25
        Caption = 'Button2'
        TabOrder = 2
        OnClick = Button2Click
      end
      object Chart9: TChart
        Left = 576
        Top = 0
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'summ_method')
        TabOrder = 3
        object Series9: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart10: TChart
        Left = 576
        Top = 264
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'Pirson')
        TabOrder = 4
        object Series10: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1051#1072#1073' 1 '#1080' 2'
      ImageIndex = 1
      object Chart1: TChart
        Left = 24
        Top = 164
        Width = 257
        Height = 161
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'Own function')
        BottomAxis.Title.Caption = 'N'
        BottomAxis.Title.Font.Charset = DEFAULT_CHARSET
        BottomAxis.Title.Font.Color = clBlack
        BottomAxis.Title.Font.Height = -16
        BottomAxis.Title.Font.Name = 'Arial'
        BottomAxis.Title.Font.Style = []
        LeftAxis.Title.Caption = 'Pseudorandom values'
        LeftAxis.Title.Font.Charset = DEFAULT_CHARSET
        LeftAxis.Title.Font.Color = clBlack
        LeftAxis.Title.Font.Height = -16
        LeftAxis.Title.Font.Name = 'Arial'
        LeftAxis.Title.Font.Style = []
        Legend.Visible = False
        View3D = False
        TabOrder = 0
        object Series1: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clRed
          Pointer.HorizSize = 1
          Pointer.InflateMargins = True
          Pointer.Style = psSmallDot
          Pointer.VertSize = 1
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart2: TChart
        Left = 288
        Top = 164
        Width = 257
        Height = 161
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'random(x)')
        BottomAxis.Title.Caption = 'N'
        BottomAxis.Title.Font.Charset = DEFAULT_CHARSET
        BottomAxis.Title.Font.Color = clBlack
        BottomAxis.Title.Font.Height = -16
        BottomAxis.Title.Font.Name = 'Arial'
        BottomAxis.Title.Font.Style = []
        LeftAxis.Title.Caption = 'Pseudorandom values'
        LeftAxis.Title.Font.Charset = DEFAULT_CHARSET
        LeftAxis.Title.Font.Color = clBlack
        LeftAxis.Title.Font.Height = -16
        LeftAxis.Title.Font.Name = 'Arial'
        LeftAxis.Title.Font.Style = []
        Legend.Visible = False
        View3D = False
        TabOrder = 1
        object Series2: TPointSeries
          Marks.ArrowLength = 0
          Marks.Visible = False
          SeriesColor = clRed
          Pointer.HorizSize = 1
          Pointer.InflateMargins = True
          Pointer.Style = psSmallDot
          Pointer.VertSize = 1
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart3: TChart
        Left = 40
        Top = 11
        Width = 217
        Height = 146
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'f(x) n=100')
        TabOrder = 2
        object Series3: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clGreen
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart4: TChart
        Left = 264
        Top = 12
        Width = 273
        Height = 145
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'f(x) n=10000')
        TabOrder = 3
        object Series4: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart5: TChart
        Left = 552
        Top = 164
        Width = 225
        Height = 145
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'standart random')
        TabOrder = 4
        object Series5: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object GroupBox1: TGroupBox
        Left = 800
        Top = 24
        Width = 185
        Height = 81
        Caption = 'Number'
        TabOrder = 5
      end
      object samplecheckRB: TRadioButton
        Left = 544
        Top = 8
        Width = 257
        Height = 17
        Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1088#1072#1074#1085#1086#1084#1077#1088#1085#1086#1089#1090#1080' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103
        TabOrder = 6
      end
      object statistindRB: TRadioButton
        Left = 544
        Top = 40
        Width = 249
        Height = 17
        Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1090#1072#1090#1080#1089#1090#1080#1095#1077#1089#1082#1086#1081' '#1085#1077#1079#1072#1074#1080#1089#1080#1084#1086#1089#1090#1080
        TabOrder = 7
      end
      object indirectRB: TRadioButton
        Left = 544
        Top = 24
        Width = 241
        Height = 17
        Caption = #1050#1086#1089#1074#1077#1085#1085#1072#1103' '#1087#1088#1086#1074#1077#1088#1082#1072
        TabOrder = 8
      end
      object gps4: TRadioButton
        Left = 544
        Top = 56
        Width = 113
        Height = 17
        Caption = #1043#1055#1057#1063
        TabOrder = 9
      end
      object Button1: TButton
        Left = 544
        Top = 96
        Width = 185
        Height = 25
        Caption = 'Generate!'
        TabOrder = 10
        OnClick = Button1Click
      end
      object Edit1: TEdit
        Left = 544
        Top = 72
        Width = 121
        Height = 21
        TabOrder = 11
      end
      object Chart6: TChart
        Left = 352
        Top = 336
        Width = 289
        Height = 153
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'statistic independence'
          '')
        TabOrder = 12
        object Series6: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1051#1072#1073'4'
      ImageIndex = 2
      object Chart11: TChart
        Left = 8
        Top = 0
        Width = 353
        Height = 169
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Alignment = taLeftJustify
        Title.Text.Strings = (
          'freq1')
        TabOrder = 0
        object Series11: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Button3: TButton
        Left = 8
        Top = 520
        Width = 353
        Height = 25
        Caption = 'Button3'
        TabOrder = 1
        OnClick = Button3Click
      end
      object Chart12: TChart
        Left = 8
        Top = 176
        Width = 353
        Height = 169
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 2
        object Series12: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Edit2: TEdit
        Left = 232
        Top = 8
        Width = 121
        Height = 21
        TabOrder = 3
        Text = #1063#1072#1089#1090#1086#1090#1072' 1'
      end
      object Edit3: TEdit
        Left = 232
        Top = 184
        Width = 121
        Height = 21
        TabOrder = 4
        Text = #1063#1072#1089#1090#1086#1090#1072' 2'
      end
      object Chart13: TChart
        Left = 552
        Top = 8
        Width = 321
        Height = 193
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'spectre')
        TabOrder = 5
        object Series14: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart14: TChart
        Left = 8
        Top = 344
        Width = 353
        Height = 177
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 6
        object Series13: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Memo1: TMemo
        Left = 440
        Top = 216
        Width = 289
        Height = 297
        TabOrder = 7
      end
      object Edit4: TEdit
        Left = 768
        Top = 256
        Width = 121
        Height = 21
        TabOrder = 8
        Text = 'Edit4'
      end
      object Edit5: TEdit
        Left = 768
        Top = 280
        Width = 121
        Height = 21
        TabOrder = 9
        Text = 'Edit5'
      end
      object Edit6: TEdit
        Left = 368
        Top = 96
        Width = 121
        Height = 21
        TabOrder = 10
        Text = 'T'
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1060#1072#1081#1083'+'#1057#1087#1077#1082#1090#1088
      ImageIndex = 3
      object Chart15: TChart
        Left = 32
        Top = 24
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'spectre from file')
        TabOrder = 0
        object Series15: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart16: TChart
        Left = 32
        Top = 280
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'data from file')
        TabOrder = 1
        object Series16: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Button4: TButton
        Left = 472
        Top = 416
        Width = 121
        Height = 25
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
        TabOrder = 2
        OnClick = Button4Click
      end
      object Memo2: TMemo
        Left = 448
        Top = 24
        Width = 185
        Height = 385
        Lines.Strings = (
          'Memo2')
        TabOrder = 3
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1048#1084#1087#1091#1083#1100#1089#1085#1086#1077' '#1085#1072#1082#1086#1087#1083#1077#1085#1080#1077' '#1089#1080#1075#1085#1072#1083#1072
      ImageIndex = 4
      object Chart17: TChart
        Left = 32
        Top = 0
        Width = 401
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 0
        object Series17: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Button5: TButton
        Left = 552
        Top = 504
        Width = 75
        Height = 25
        Caption = 'Button5'
        TabOrder = 1
        OnClick = Button5Click
      end
      object Chart18: TChart
        Left = 472
        Top = 0
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 2
        object Series18: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart19: TChart
        Left = 32
        Top = 248
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 3
        object Series19: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart20: TChart
        Left = 464
        Top = 248
        Width = 400
        Height = 250
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'TChart')
        TabOrder = 4
        object Series20: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Edit7: TEdit
        Left = 112
        Top = 504
        Width = 121
        Height = 21
        TabOrder = 5
        Text = #1063#1072#1089#1090#1086#1090#1072
      end
      object Edit8: TEdit
        Left = 256
        Top = 504
        Width = 121
        Height = 21
        TabOrder = 6
        Text = #1055#1077#1088#1080#1086#1076
      end
      object Edit9: TEdit
        Left = 408
        Top = 504
        Width = 129
        Height = 21
        TabOrder = 7
        Text = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1080#1084#1087#1091#1083#1100#1089#1086#1074
      end
    end
    object TabSheet6: TTabSheet
      Caption = #1051#1072#1073'6'
      ImageIndex = 5
      object Chart21: TChart
        Left = 0
        Top = 0
        Width = 497
        Height = 153
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1057#1080#1075#1085#1072#1083#8470'1')
        TabOrder = 0
        object Series21: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart22: TChart
        Left = 496
        Top = 0
        Width = 504
        Height = 153
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1057#1080#1075#1085#1072#1083#8470'2')
        TabOrder = 1
        object Series22: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart23: TChart
        Left = 0
        Top = 344
        Width = 1001
        Height = 178
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1057#1080#1075#1085#1072#1083' '#8470'2 '#1087#1086#1089#1083#1077' '#1092#1080#1083#1100#1090#1088#1072#1094#1080#1080)
        TabOrder = 2
        object Series24: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart24: TChart
        Left = 352
        Top = 520
        Width = 649
        Height = 137
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1047#1072#1074#1080#1089#1080#1084#1086#1089#1090#1100' '#1082#1086#1101#1092' '#1082#1086#1088#1088#1077#1083#1103#1094#1080#1080
          ' '#1086#1090' '#1088#1072#1079#1084#1077#1088#1072' '#1086#1082#1085#1072' '#1089#1082#1086#1083#1100#1079#1103#1097#1077#1075#1086' '#1089#1088#1077#1076#1085#1077#1075#1086)
        TabOrder = 3
        object Series25: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Chart25: TChart
        Left = 0
        Top = 152
        Width = 1001
        Height = 193
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          #1057#1080#1075#1085#1072#1083' '#8470'1 '#1087#1086#1089#1083#1077' '#1092#1080#1083#1100#1090#1088#1072#1094#1080#1080)
        TabOrder = 4
        object Series23: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object Edit10: TEdit
        Left = 8
        Top = 552
        Width = 225
        Height = 21
        TabOrder = 5
        Text = #1056#1072#1079#1084#1077#1088' '#1086#1082#1085#1072' '#1089#1082#1086#1083#1100#1079#1103#1097#1077#1075#1086' '#1089#1088#1077#1076#1085#1077#1075#1086
      end
      object Button6: TButton
        Left = 8
        Top = 600
        Width = 225
        Height = 25
        Caption = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100' '#1089#1080#1075#1085#1072#1083' '#1080' '#1086#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100
        TabOrder = 6
        OnClick = Button6Click
      end
      object Edit11: TEdit
        Left = 8
        Top = 528
        Width = 121
        Height = 21
        TabOrder = 7
        Text = #1063#1072#1089#1090#1086#1090#1072' '#1089#1080#1075#1085#1072#1083#1086#1074
      end
      object Edit12: TEdit
        Left = 8
        Top = 576
        Width = 121
        Height = 21
        TabOrder = 8
        Text = 'T'
      end
    end
  end
end
