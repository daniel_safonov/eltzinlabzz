//---------------------------------------------------------------------------
#define _USE_MATH_DEFINES

#include <vcl.h>
#include <math.h>
#include <cmath>

#include <iostream>
#include <fstream>
#include <iomanip>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

const  long M = 41367102;
const long k = 167821;
const long b = 13141;
float r_start = 4625;
float r = 0;

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        Series1->Clear();
        Series2->Clear();
        Series3->Clear();
        Series4->Clear();
        Series5->Clear();
        //countn();

        //puasson();

        //?????????? ???? ??????:
        if(gps4->Checked){
                int N = StrToFloat(Edit1->Text);
                ourrandomToMemo(N);
        }
        //??? ???????? ????????????? ?????????????
        if(samplecheckRB->Checked){
                freqcheck(100,Series3);
                freqcheck(10000,Series4);
                freqcheckdefaultrand(10000,Series5);
                Chart3->CleanupInstance();
        }
        //????????? ???????? ?????????????
        if (indirectRB->Checked){
                indirectcheck(100);
                indirectcheck(1000);
                indirectcheck(10000);
        }
        //???????? ?????????????? ?????????????
        if (statistindRB->Checked) {
                for (int i = 10; i<1000;i++){
                        Series6->AddXY(i,statisticindepend(i));
                }
        }
         Memo->Lines->Add("===============");
}

void TForm1::ourrandomToMemo(int n)
{
         for (int i = 0; i < n; i++) {
                Series2->AddXY(i,(double)random(10000)/10000);
                r = (k * (int)r_start + b)%M;
                r_start = r;
                if (r < 0){
                        r=-r;
                        Series1->AddXY(i,r/M);
                        Memo->Lines->Add(r/M);
                }
                else{
                        Series1->AddXY(i,r/M);
                        Memo->Lines->Add(r/M);
                }
        }
}

double TForm1::matozhid(float* mymass, int g,int count)
{
        float sg = 0;
        for (int i = 0; i<count;i=i+g){
                        sg = sg + mymass[i-g]*mymass[i];
        }
        return sg/(count-g);
}

float TForm1::correlation(float* massiv,int g,int count)
{
        return 12*matozhid(massiv,g,count)-3;
}

float TForm1::statisticindepend(int count)
{       //max :100000  - more StackIsOverflow
        float *massiv = new float [count];
        for (int i = 0; i < count; i++) {
                r = (k * (int)r_start + b)%M;
                r_start = r;
                if (r > 0){
                        massiv[i]=r/M;
                }
                else{
                        r=-r;
                        massiv[i]=r/M;
                }
        }
       // Memo->Lines->Add("For n= "+IntToStr(count));
        if (count==16){
        int sososo=0;
        }
        float corr = correlation(massiv,1,count);

        //Memo->Lines->Add("Rg: "+FloatToStr(corr));
        return corr;
}


void TForm1::indirectcheck(int n)
{
        float mat=1;
        float disp=1;
        for (int i = 0; i < n; i++) {
                r = (k * (int)r_start + b)%M;
                r_start = r;
                if (r > 0){
                        mat+=r/M;
                        disp+=r*r/M/M;
                }
                else
                        {
                        r=-r;
                        mat+=r/M;
                        disp+=r*r/M/M;
                }
        }
        mat=mat/n; // c ?????? n ?????? ????????? ? 1/2
        disp=disp/n-mat*mat; // c ?????? n ?????? ????????? ? 1/12
        Memo->Lines->Add("??? n= "+IntToStr(n));
        Memo->Lines->Add("??? ????????: "+FloatToStr(mat));
        Memo->Lines->Add("?????????: "+FloatToStr(disp));
}



void TForm1::freqcheck(int n,TFastLineSeries *series)
{
        float dist = 0.1;
        float quantity [10]={0};         //for storage for quantity of n-s
        int position =0;
        for (int i = 0; i < n; i++){
                r = (k * (int)r_start + b)%M;
                r_start = r;
                if (r > 0){
                        position =  (int)(r/(dist*M));

                        quantity[position]+=1;
                }
                else{
                        r=-r;
                        position =  (int)(r/(dist*M));
                        quantity[position]+=1;
                }
        }

        for (int i = 0; i<10;i++)
                {
                series->AddXY(i,quantity[i]/n);
        }
}

void TForm1::freqcheckdefaultrand(int n,TFastLineSeries *series)
{
        float quantity [10]={0};
        int position =0;
        for (int i = 0; i < n; i++){
                        r=(double)random(n)/n;
                        if (r > 0){
                                position =  (int)(r*10);
                                quantity[position]+=1;
                        }
                        else{
                                r=-r;
                                position =  (int)(r*10);
                                quantity[position]+=1;
                        }
        }

        for (int i = 0; i<10;i++)
                {
                series->AddXY(i,quantity[i]/n);
                }
}



//---------------------------------------------------------------------------


//---------------------------------------------------------------------------


void TForm1::countn()
{
float s=0;
for (int i =0;i<12;i++)
{
r = (k * (int)r_start + b)%M;
                r_start = r;
                if (r < 0)
                {
                        r=-r;

         }
         s+=r/M;
}

s=s-6;
     Memo->Lines->Add(s);
}

void TForm1::puasson()
{
double Pi,rand_number;
double H[20]={0};
int i;
for (int a=0;a<10000;a++){
        rand_number = (k * (int)r_start + b)%M;
        r_start = rand_number;
        if (rand_number < 0){
                rand_number=-rand_number;
         }
         rand_number=rand_number/M;
   Pi=0;
   i=0;
   while (rand_number > Pi)
{
   Pi+=(double)(pow(4,i)*pow(2.71,-4)/factorial(i));
     i++;
}
   Series8->AddXY(i,rand_number);
   H[i]++;
}
   for(int i=0;i<20;i++)
{
   Series7->AddXY(i,H[i]/10000);
}
}

long double TForm1::factorial(int N)
{
       if(N < 0)
        return 0;
    if (N == 0)
        return 1;
    else
        return N * factorial(N - 1);
}

long double TForm1::mypow(int osn, int deg)
{
long double result =1;
        if (deg>0){
        for (int i =0;i<deg;i++){
              result*=osn;
        }
        }
        if (deg<0) {
         deg=-deg;
                for (int i =0;i<deg;i++){
                        result=result/osn;
                }
        }
        if (deg==0){
        result=1;
        }
        return result;
}
void __fastcall TForm1::Button2Click(TObject *Sender)
{
puasson();
summMethod();
pirson_form();
}
//---------------------------------------------------------------------------


void TForm1::summMethod()
{
float quantity [20]={0};
double massiv[10000]={0};
long double rand_number;
int position =0;
long double x_normir=0;
long double x_norm=0;
long double s_temp=0;
    for (int i=0;i<10000;i++){
    s_temp=0;
        for (int j=0;j<12;j++){
            rand_number = (k * (int)r_start + b)%M;
            r_start = rand_number;
            if (rand_number < 0){
                rand_number=-rand_number;
            }
            rand_number=rand_number/M;
            s_temp=s_temp+(double)random(10000)/10000;
        }

        x_normir=s_temp-6;
        x_norm=4*x_normir+4;//k=4; M=4;D=16
        massiv[i]=x_norm;
    }
//well-look on chart
//diapason
float min=0;
float max=0;
    for (int i=0;i<10000;i++){
        if (max<massiv[i]){
        max=massiv[i];
        }
        if (min>massiv[i]){
        min=massiv[i];
        }
    }
double step = (max-min)/10;
    for (int t=0; t<10; t++){
        int n=0;
        for ( int k=0; k<10000; k++){
            if ((massiv[k]>min+t*step) && (massiv[k]<=min+(1+t)*step))
            n++;
        }
        Series9->AddXY(t,(double)n/10000);
    }
}

void TForm1::pirson_form()
{
float quantity [20]={0};
long double rand_number;
int position =0;
double massiv[10000]={0};
long double x_normir=0;
long double x_norm=0;
long double s_temp=0;
    for (int i=0;i<10000;i++){
    s_temp=0;
        for (int j=0;j<12;j++){
            rand_number = (k * (int)r_start + b)%M;
            r_start = rand_number;
            if (rand_number < 0){
                rand_number=-rand_number;
            }
            rand_number=rand_number/M;
            s_temp=s_temp+(double)random(10000)/10000;
        }
        x_normir=s_temp-6;
        x_norm=4*x_normir+4;//k=4; M=4;D=16
        massiv[i]=x_normir;
    }
double X[2500];
int l=0;
    for (int i=0;i<10000;i+=4){
        X[l] = pow(massiv[i],2)+pow(massiv[i+1],2)+pow(massiv[i+2],2)+pow(massiv[i+3],2);
        l++;
}
//well-look on chart
//diapason
float min=0;
float max=0;
    for (int i=0;i<2500;i++){
        if (max<X[i]){
        max=X[i];
        }
        if (min>X[i]){
        min=X[i];
        }
    }
int step = (max-min)/10;
   for (int t=0; t<10; t++){
        int n=0;
        for ( int k=0; k<2500; k++){
            if ((X[k]>t*step) && (X[k]<=1+t*step)){
                n++;
                        }
        }
            Series10->AddXY(t,(double)n/2500);
    }
}
//lab4
void __fastcall TForm1::Button3Click(TObject *Sender)
{
Chart11->Repaint();
Chart12->Repaint();
Chart14->Repaint();
 int freq1 = StrToInt(Edit2->Text);
  int freq2 = StrToInt(Edit3->Text);
  int T =StrToInt(Edit6->Text);
  signal_gen(freq1,freq2,T);
  spectre(freq1,freq2,T);
}
//---------------------------------------------------------------------------




void TForm1::signal_gen(int freq1, int freq2, int T){   
double first_garm;
double sec_garm;
double summ_garm;
   for (float i=0;i<T;i=i+0.1){
    first_garm = sin(2*3.14*i/T*freq1);
    sec_garm = sin(2*3.14*i/T*freq2);
    summ_garm = first_garm+sec_garm;
        Memo1->Lines->Add(first_garm);
    Series11->AddXY(i,first_garm);
    Series12->AddXY(i,sec_garm);
    Series13->AddXY(i,summ_garm);
    }
        Memo1->Lines->SaveToFile("signal.txt");
}

void TForm1::spectre(int freq1,int freq2, int T){
double summ_garm=0;
    for (int sin_freq=1;sin_freq<=100;sin_freq++){
        summ_garm=0;
        for (float i=0;i<T;i=i+0.1) {
            //summ_garm+= sin(2*3.14*i/T*freq1)*sin(2*3.14*i/T*sin_freq); //one peak
            summ_garm+= (sin(2*3.14*i/T*freq1)+sin(2*3.14*i/T*freq2))*sin(2*3.14*i/T*sin_freq); //double peak
            //summ_garm+= (sin(2*3.14*i/T*freq1)+sin(2*3.14*i/T*freq1*2)+sin(2*3.14*i/T*freq1*0.5))*sin(2*3.14*i/T*sin_freq); //triple peak
        }
    Series14->AddXY(sin_freq,abs((int)summ_garm));
    }
 }

void TForm1::spectre_file(int T){
double summ_garm=0;
float j=0;
    for (int sin_freq=1;sin_freq<=100;sin_freq++){
        summ_garm=0;
        for (int i = 0; i < Memo2->Lines->Count; i++) {
                summ_garm+=StrToFloat(Memo2->Lines->Strings[i])*sin(2*3.14*j/T*sin_freq);
                j+=0.1;
        }
    Series15->AddXY(sin_freq,abs((int)summ_garm));
    }
}
void __fastcall TForm1::Button4Click(TObject *Sender){
    Memo2->Lines->LoadFromFile("signal.txt");
    spectre_file(128);
        generate_file();
}

void TForm1::generate_file()
{
for (int i = 0; i < Memo2->Lines->Count; i++) {
        Series16->AddXY(i,StrToFloat(Memo2->Lines->Strings[i]));
        }
}
//----------------------------------------------------------------------
//labour work #5
//14.03.16
void __fastcall TForm1::Button5Click(TObject *Sender)
{
Series17->Clear();
Series18->Clear();
Series19->Clear();
Series20->Clear();
int freq = StrToInt(Edit7->Text);
int T =StrToInt(Edit8->Text);
int impulse_count = StrToInt(Edit9->Text);
double *noise;
double *sin_signal;
double *noise_and_sin_signal;
noise=noise_generator(10000);
sin_signal=sin_generator(freq,T);
noise_and_sin_signal=sin_and_noise(noise,sin_signal,T);
accumulation(noise_and_sin_signal,T,impulse_count);
arrayToSeries(noise,Series17,10000);
arrayToSeries(sin_signal,Series19,T*10);
arrayToSeries(noise_and_sin_signal,Series18,T*10);
}

double * TForm1::noise_generator(int count){
 double *noise= new double[count];
int md=1;
    for (int i=0; i < count; i++){
        noise[i]=(float)random(count)/count*md;
        }
return noise;
}
double * TForm1::sin_generator(int freq, int T){
 double *sin_signal = new double [T*10];
int j=0;
    for (float i=0;i<T;i=i+0.1){
        sin_signal[j] = sin(2*3.14*i/T*freq)*1;
        if (sin_signal[j]<0){
            sin_signal[j]=0;  
        }
        j+=1;
    }
    return sin_signal;
}

double * TForm1::sin_and_noise(double* noise,double* sin_signal,int T){
    double *new_signal = new double [T*10];
    for (int i = 0; i < T*10; i++){
        new_signal[i]=sin_signal[i]+noise[i];
    }
return new_signal; 
}




void TForm1::accumulation(double * noise_and_sin_signal, int T,int impulse_count){
double *accumulated = new double [T/4];
double S = 0;
    for (int i = 0; i < 5120; i++) {
        noise_and_sin_signal[i]=noise_and_sin_signal[i]/impulse_count;
    }
    for (float j=0; j < T/4; j++){
        for (int i=j; i < 256*impulse_count; i+=256){
        S=S+noise_and_sin_signal[i];
        }
    Series20->AddXY(j,S);
    S=0;
    }
}



//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
int freq = StrToInt(Edit11->Text);
int window = StrToInt(Edit10->Text);
int T = StrToInt(Edit12->Text);
double *noise1,*noise2;
double *sin_signal1,*sin_signal2;
double *noise_and_sin_signal1,*noise_and_sin_signal2;
double *filtered1,*filtered2;

noise1=noise_generator(10000); // generating noise 1
noise2=noise_generator(5000); // generating noise 2

sin_signal1=sin_generator(freq,T);//generating a signal with freq and T
sin_signal2=sin_generator(freq,T);
noise_and_sin_signal1=sin_and_noise(noise1,sin_signal1,T);//signal 1 with noise
arrayToSeries(noise_and_sin_signal1,Series21,T*10); // put on chart1
noise_and_sin_signal2=sin_and_noise(noise2,sin_signal2,T);//signal 2 with noise
arrayToSeries(noise_and_sin_signal2,Series22,T*10); // put on chart2

dependenceCorrOnWindow(noise_and_sin_signal1,noise_and_sin_signal2,T); // put corr dependence on chart

filtered1=window_average(noise_and_sin_signal1,window,T);
filtered2=window_average(noise_and_sin_signal2,window,T);

arrayToSeries(filtered1,Series23,T*10); // put on filtered signal 1
arrayToSeries(filtered2,Series24,T*10); // put on filtered signal 2
}

void TForm1::arrayToSeries(double* actual_array,
                            TFastLineSeries *series,int count){
    for (int i=0;i<count;i++){
        series->AddXY(i,actual_array[i]);
    }
}

double * TForm1::window_average(double* actual_signal,int window,int T){
float avg_sum=0;
double avg=0;
double *actual_signal_avg = new double [T*10];
    for (int i=0;i<T*10;i++){
        for (int j=i;j<i+window;j++){
            avg_sum+=actual_signal[i];
        }
        avg=double(avg_sum)/double(window);
    actual_signal_avg[i]=avg;
    avg_sum=0;
    }
return actual_signal_avg;
}

double TForm1::correlPirson(double* signal1,double* signal2,int count){
double avg1=avgInArray(signal1,count);
double avg2=avgInArray(signal2,count);
double corr=0;
double xsum=0;
double ysum=0;
double numerator=0;
double denominator=0;
    for (int i=0;i<count;i++){
        numerator+=(signal1[i]-avg1)*(signal2[i]-avg2);
        xsum+=pow((signal1[i]-avg1),2);
        ysum+=pow((signal2[i]-avg2),2);
    }
    denominator=sqrt(xsum*ysum);
    corr=numerator/denominator;
return corr;
}
void TForm1::dependenceCorrOnWindow(double* signal1,double* signal2, int T){
double *signal_after_average1;
double *signal_after_average2;
double corr=0;  
    for (int i=1;i<100;i++){
        signal_after_average1=window_average(signal1,i,T);//signal 1 through filter with window = i
        signal_after_average2=window_average(signal2,i,T); //signal 2 through filter with window = i 
        corr=correlPirson(signal_after_average1,signal_after_average2,T*10);
        Series25->AddXY(i,corr);
    }
}

double TForm1::avgInArray(double* signal1,int count){
double sum=0;
for (int i =0;i<count;i++){
    sum+=signal1[i];
}
sum=sum/count;
return sum;
}


