//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo;
        TPageControl *PageControl1;
        TTabSheet *TabSheet1;
        TTabSheet *TabSheet2;
        TChart *Chart1;
        TPointSeries *Series1;
        TChart *Chart2;
        TPointSeries *Series2;
        TChart *Chart3;
        TFastLineSeries *Series3;
        TChart *Chart4;
        TFastLineSeries *Series4;
        TChart *Chart5;
        TFastLineSeries *Series5;
        TGroupBox *GroupBox1;
        TRadioButton *samplecheckRB;
        TRadioButton *statistindRB;
        TRadioButton *indirectRB;
        TRadioButton *gps4;
        TButton *Button1;
        TEdit *Edit1;
        TChart *Chart6;
        TFastLineSeries *Series6;
        TChart *Chart7;
        TFastLineSeries *Series7;
        TChart *Chart8;
        TButton *Button2;
        TChart *Chart9;
        TFastLineSeries *Series9;
        TChart *Chart10;
        TFastLineSeries *Series10;
        TPointSeries *Series8;
        TTabSheet *TabSheet3;
        TChart *Chart11;
        TButton *Button3;
        TChart *Chart12;
        TEdit *Edit2;
        TEdit *Edit3;
        TChart *Chart13;
        TChart *Chart14;
        TMemo *Memo1;
        TEdit *Edit4;
        TEdit *Edit5;
        TFastLineSeries *Series11;
        TFastLineSeries *Series12;
        TFastLineSeries *Series13;
        TEdit *Edit6;
        TFastLineSeries *Series14;
        TTabSheet *TabSheet4;
        TChart *Chart15;
        TChart *Chart16;
        TButton *Button4;
        TMemo *Memo2;
        TFastLineSeries *Series15;
        TFastLineSeries *Series16;
        TTabSheet *TabSheet5;
        TChart *Chart17;
        TButton *Button5;
        TChart *Chart18;
        TChart *Chart19;
        TChart *Chart20;
        TFastLineSeries *Series17;
        TFastLineSeries *Series18;
        TFastLineSeries *Series19;
        TFastLineSeries *Series20;
        TEdit *Edit7;
        TEdit *Edit8;
        TEdit *Edit9;
        TTabSheet *TabSheet6;
        TChart *Chart21;
        TChart *Chart22;
        TChart *Chart23;
        TChart *Chart24;
        TChart *Chart25;
        TEdit *Edit10;
        TButton *Button6;
        TFastLineSeries *Series21;
        TFastLineSeries *Series22;
        TFastLineSeries *Series23;
        TFastLineSeries *Series24;
        TFastLineSeries *Series25;
        TEdit *Edit11;
        TEdit *Edit12;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        
        
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
        void freqmethod(int n);
        int ourrandom(int n);
        void ourrandomToMemo(int n);
        void frequencycheck(int n, int kolvoint);
        void indirectcheck(int n);
        float statisticindepend(int count);
        void freqcheck(int n,TFastLineSeries *series);
        void freqcheckdefaultrand(int n,TFastLineSeries *series);
        double matozhid(float* mymass, int g,int count);
        float correlation(float* massiv,int g,int count);
        void countn();
        void puasson();
        long double factorial(int N);
        long double mypow(int osn, int deg);
        void summMethod();
        void pirson_form();
        void signal_generator(int freq);
        void signal_gen(int freq1, int freq2, int T);
        void spectre(int freq1, int freq2, int T);
        void spectre_file(int T);
        void generate_file();
        double * noise_generator(int count);
        double* sin_generator(int freq, int T);
        double* sin_and_noise(double* noise,double* sin_signal,int T);
        void accumulation(double * noise_and_sin_signal, int T,int impulse_count);
        double TForm1::avgInArray(double* signal1,int count);
        void TForm1::dependenceCorrOnWindow(double* signal1,double* signal2, int T);
        double TForm1::correlPirson(double* signal1,double* signal2,int count);
        double * TForm1::window_average(double* actual_signal,int avg_size,int T);
        void TForm1::arrayToSeries(double* actual_array,TFastLineSeries *series,int count);

};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
