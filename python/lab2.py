import matplotlib.pyplot as plt
import numpy as np
from numpy import array
from random import random
import sys 

M = 2147483647

def genRandom():
    k = 16807
    b = 13337
    r = 1213
    while True:
        r = k*r+b
        ri=r%M
        r= ri
        ri = (float(ri)/M)
        yield ri

def calc_mx(array, g, N):
    sg = sum([array[i-g]*array[i] for i in xrange(g, N, g)])
    return sg/(N-g)


def corr(array, g, N):
    return 12*calc_mx(array, g ,N)-3

def freq_check(N, mx):
    delta = 0.1
    quant = np.zeros(N)
    for i in xrange(N):
        position = mx[i]/(delta*M)
        quant[position] += 1
    return quant


N = 1000
START = 50
gen = genRandom()
mxArray = []
dxArray = []
sumMxArray = np.array([])
sumDxArray = np.array([])
for i in xrange(N):
    a = next(gen)
    mxArray.append(a)
    dxArray.append(a*a)
    mx = sum(mxArray)/N
    sumMxArray = np.append(sumMxArray, mx)
    sumDxArray = np.append(sumDxArray, sum(dxArray)/N - (mx*mx))

"""
print "Mx: "+str(mx)
print "Dx: "+str(dx)
"""
if '1' in sys.argv: 
    plt.figure(1)
    plt.subplot(211)
    plt.ylabel('Mx')
    plt.plot(xrange(N), sumMxArray,  'r-')
    plt.subplot(212)
    plt.ylabel('Dx')
    plt.plot(xrange(N), sumDxArray, 'b-')
    plt.show()


if '0' in sys.argv:
    

    plt.figure(1)
    plt.subplot(211)
    plt.ylabel('My generator')
    plt.plot(xrange(N), mxArray, 'ro')
    plt.subplot(212)
    plt.ylabel('Built-in generator')
    plt.plot(xrange(N), [random() for i in xrange(N)], 'bo')
    plt.show()

if '2' in sys.argv:
    corrArr = array([])
    for i in xrange(START, N):
        corrArr = np.append(corrArr, corr(mxArray, 1, i))

    print corrArr
    plt.plot(xrange(0, N-START), corrArr, '-')
    plt.show()

if '3' in sys.argv:
    ar = freq_check(N, mxArray)
    plt.plot(xrange(len(mxArray)), mxArray, '-')
    plt.show()
